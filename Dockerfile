FROM docker:19.03.14-dind

ENV KIND_VERSION=0.9.0 \
    KIND_LOGLEVEL=info \
    IPTABLES_VERSION=1.8.4-r2 \
    CURL_VERSION=7.76.1-r0 \
    GO_VERSION=1.13.15-r0 \
    LIBC_DEV_VERSION=0.7.2-r3 \
    KUBECTL_VERSION=1.17.0 \
    CONFD_VERSION=0.16.0 \
    API_SERVER_ADDRESS=0.0.0.0 \
    API_SERVER_PORT=8443 \
    DOCKERD_PORT=2375

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

VOLUME /data

COPY ./resources /resources

RUN /resources/build && rm -rf /resources

WORKDIR /data

ENTRYPOINT ["entrypoint"]

LABEL "maintainer"="wild-beavers" \
      "org.label-schema.name"="kind" \
      "org.label-schema.base-image.name"="docker.io/library/docker" \
      "org.label-schema.base-image.version"="19.03.5-dind" \
      "org.label-schema.description"="Kind in a container" \
      "org.label-schema.url"="https://github.com/kubernetes-sigs/kind/" \
      "org.label-schema.vcs-url"="https://gitlab.com/wild-beavers/docker/kind" \
      "org.label-schema.vendor"="Wild-Beavers" \
      "org.label-schema.schema-version"="1.0.0-rc.1" \
      "org.label-schema.applications.kind.version"=$KIND_VERSION \
      "org.label-schema.vcs-ref"=$VCS_REF \
      "org.label-schema.version"=$VERSION \
      "org.label-schema.build-date"=$BUILD_DATE \
      "org.label-schema.usage"=""
